package com.example.myapplication.Interface;

import com.example.myapplication.model.City;
import com.example.myapplication.model.ListCities;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {

    @GET("weather")
    Call<City> getCity(@Query("q") String city, @Query("appid") String key);

    @GET("box/city")
    Call<ListCities> getCitiesInBox(@Query("bbox") String box, @Query("appid") String key);
}
