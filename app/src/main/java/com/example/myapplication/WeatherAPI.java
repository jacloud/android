package com.example.myapplication;

import android.widget.Toast;

import com.example.myapplication.Interface.WeatherService;
import com.example.myapplication.model.City;
import com.example.myapplication.model.ListCities;

import java.net.URL;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherAPI {

    private static final String URL = "http://api.openweathermap.org/data/2.5/";
    private static final String KEY = "780545f44f4e769a504cb9633218ce37";
    private static WeatherService weatherService;

    private static City cityObject;

    private static WeatherService getWeatherService() {
        if(weatherService == null) {
            Retrofit retrofit = new Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create()).build();
            weatherService = retrofit.create(WeatherService.class);
        }
        return weatherService;
    }

    public static Call<City> getCity(String city) {

        return getWeatherService().getCity(city, KEY);

    }

    public static Call<ListCities> getCitiesInBox(String box) {

        return getWeatherService().getCitiesInBox(box, KEY);

    }
}
