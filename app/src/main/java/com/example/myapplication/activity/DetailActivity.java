package com.example.myapplication.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.model.Constants;
import com.example.myapplication.model.Mail;
import com.example.myapplication.model.MailDetailViewModel;

public class DetailActivity extends AppCompatActivity {

    private MailDetailViewModel model;
    private TextView title;
    private TextView subject;
    private TextView subjectIcon;
    private TextView message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);

        Bundle b = getIntent().getExtras();
        String title = b.getString(Constants.TITLE);
        String subject = b.getString(Constants.SUBJECT);
        String message = b.getString(Constants.MESSAGE);

        ViewModelProviders.of(this).get(MailDetailViewModel.class).select(new Mail(title, subject, message));
    }
}
