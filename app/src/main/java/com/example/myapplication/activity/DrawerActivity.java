package com.example.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;

import com.example.myapplication.Interface.OnListFragmentInteractionListener;
import com.example.myapplication.model.Constants;
import com.example.myapplication.model.Mail;
import com.example.myapplication.model.MailDetailViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.View;

import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;

import com.example.myapplication.R;

public class DrawerActivity extends AppCompatActivity implements OnListFragmentInteractionListener {

    private AppBarConfiguration mAppBarConfiguration;
    private MailDetailViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_mail, R.id.nav_http)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        model = ViewModelProviders.of(this).get(MailDetailViewModel.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onListFragmentInteraction(Mail mail) {

        if( findViewById(R.id.mailDetailFragment) == null) {
            Intent i = new Intent(DrawerActivity.this, DetailActivity.class);
            i.putExtra(Constants.TITLE, mail.getTitle());
            i.putExtra(Constants.SUBJECT, mail.getSubject());
            i.putExtra(Constants.MESSAGE, mail.getMessage());
            startActivity(i);
        } else {
            model.select(mail);
        }

    }
}
