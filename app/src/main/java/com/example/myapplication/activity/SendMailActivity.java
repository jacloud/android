package com.example.myapplication.activity;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.example.myapplication.model.Constants;
import com.example.myapplication.model.Mail;
import com.example.myapplication.myApp.MyApp;
import com.example.myapplication.myApp.NotificationHandler;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.R;

import java.util.HashMap;
import java.util.Map;

public class SendMailActivity extends AppCompatActivity {

    private NotificationHandler notificationHandler;

    public static int notificationID = 0;
    public static Map<String, Integer> notificationGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_mail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        EditText subject = findViewById(R.id.subject);
        EditText title = findViewById(R.id.title);
        EditText message = findViewById(R.id.message);

        notificationHandler = new NotificationHandler(this);

        notificationGroup = MyApp.getNotificationGroup();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            boolean error = false;
            if(subject.getText().length() == 0) {
                Toast.makeText(SendMailActivity.this, R.string.empty_receiver, Toast.LENGTH_SHORT).show();
                error = true;
            }

            if(!error) {
                Mail mail = new Mail(title.getText().toString(), subject.getText().toString(), message.getText().toString());
                MyApp.getMailList().add(mail);
                Intent detailIntent = new Intent(this, DetailActivity.class);
                detailIntent.putExtra(Constants.TITLE, mail.getTitle());
                detailIntent.putExtra(Constants.SUBJECT, mail.getSubject());
                detailIntent.putExtra(Constants.MESSAGE, mail.getMessage());
                detailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, notificationID, detailIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                Notification.Builder notificationbuilder = notificationHandler.createNotificationWithIntent(mail.getTitle(), mail.getMessage(), NotificationHandler.CHANNEL_ID.HIGH, pendingIntent);
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH)  {
                    String currentSubject = mail.getSubject();
                    notificationbuilder.setGroup(currentSubject);
                    int idGroup = 0;
                    if(notificationGroup.containsKey(currentSubject))  {
                        idGroup = notificationGroup.get(currentSubject);
                    } else {
                        idGroup = notificationGroup.size();
                        notificationGroup.put(currentSubject, idGroup);
                    }
                    notificationHandler.getManger().notify(idGroup, notificationHandler.createNotificationGroup(mail.getSubject(), NotificationHandler.CHANNEL_ID.LOW.toString()).build());
                }
                notificationHandler.getManger().notify(notificationID, notificationbuilder.build());
                notificationID++;
                Intent intent = new Intent(SendMailActivity.this, DrawerActivity.class);
                startActivity(intent);
            }

        });
    }

}
