package com.example.myapplication.adapter;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.model.City;
import com.example.myapplication.model.Constants;

import java.util.List;

/**
 * TODO: Replace the implementation with code for your data type.
 */
public class HttpAdapter extends RecyclerView.Adapter<HttpAdapter.ViewHolder> {

    private final List<City> mValues;

    public HttpAdapter(List<City> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_http, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        switch (mValues.get(position).getWeather().get(0).getMain()) {
            case Constants.SUN:
                holder.weatherImage.setImageResource(R.mipmap.sun);
                break;
            case Constants.CLOUD:
                holder.weatherImage.setImageResource(R.mipmap.cloud);
                break;
            case Constants.RAIN:
                holder.weatherImage.setImageResource(R.mipmap.rain);
                break;
            default:
                holder.weatherImage.setImageResource(R.mipmap.sun);
                break;
        }
        holder.mContentView.setText(mValues.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView weatherImage;
        public final TextView mContentView;
        public City mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            weatherImage = (ImageView) view.findViewById(R.id.weatherImage);
            mContentView = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
