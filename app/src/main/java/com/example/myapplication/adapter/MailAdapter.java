package com.example.myapplication.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Interface.OnListFragmentInteractionListener;
import com.example.myapplication.R;
import com.example.myapplication.model.Mail;

import java.util.List;

public class MailAdapter extends RecyclerView.Adapter<MailAdapter.MyViewHolder> {

    private List<Mail> mailList;
    private final OnListFragmentInteractionListener mListener;

    public MailAdapter(List<Mail> mailList, OnListFragmentInteractionListener listener) {
        this.mailList = mailList;
        mListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mail_list_layout, parent, false);

        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Mail mail = mailList.get(position);
        holder.subjectIcon.setText(mail.getSubject().substring(0,1));
        holder.subject.setText(mail.getSubject());
        holder.title.setText(mail.getTitle());
        holder.message.setText(mail.getMessage());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(mail);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mailList.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public final View mView;
        public TextView subjectIcon;
        public TextView subject;
        public TextView title;
        public TextView message;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.mView = itemView;
            this.subjectIcon = itemView.findViewById(R.id.iconSubject);
            this.subject = itemView.findViewById(R.id.sendTo);;
            this.title = itemView.findViewById(R.id.title);
            this.message = itemView.findViewById(R.id.message);;
        }

    }

}
