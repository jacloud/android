package com.example.myapplication.fragment;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.model.MailDetailViewModel;

public class MailDetailFragment extends Fragment {

    private MailDetailViewModel mViewModel;

    private View view;

    public static MailDetailFragment newInstance() {
        return new MailDetailFragment();
    }

    private TextView title;
    private TextView subject;
    private TextView subjectIcon;
    private TextView message;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.mail_detail_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(MailDetailViewModel.class);
        // TODO: Use the ViewModel

        title = (TextView) view.findViewById(R.id.title);
        subject = (TextView) view.findViewById(R.id.sendTo);
        subjectIcon = (TextView) view.findViewById(R.id.iconSubject);
        message = (TextView) view.findViewById(R.id.message);

        mViewModel.getSelected().observe(this, selected -> {
            // Update the UI.
            title.setText(selected.getTitle());
            subjectIcon.setText(selected.getSubject().substring(0,1));
            subjectIcon.setVisibility(View.VISIBLE);
            subject.setText(selected.getSubject());
            message.setText(selected.getMessage());
        });
    }

}
