package com.example.myapplication.model;

import java.util.List;

public class City {

    private int id;
    private String name;
    private List<Weather> weather;

    public City() {
    }

    public City(int id, String name, List<Weather> weather) {
        this.id = id;
        this.name = name;
        this.weather = weather;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }
}
