package com.example.myapplication.model;

public class Constants {

    public static final String TITLE = "title";
    public static final String SUBJECT = "subject";
    public static final String MESSAGE = "message";

    public static final String SUN = "Clear";
    public static final String CLOUD = "Clouds";
    public static final String RAIN = "Rain";
}
