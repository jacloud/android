package com.example.myapplication.model;

import java.util.List;

public class ListCities {

    public ListCities() {

    }

    public ListCities(List<City> list) {
        this.list = list;
    }

    private List<City> list;

    public List<City> getList() {
        return list;
    }

    public void setList(List<City> list) {
        this.list = list;
    }
}
