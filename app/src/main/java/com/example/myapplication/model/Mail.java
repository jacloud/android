package com.example.myapplication.model;

import android.graphics.Color;

import java.util.List;

public class Mail {

    private String title;
    private String message;
    private String subject;
    private List<String> senders;
    private Color color;

    public Mail() {

    }

    public Mail(String title, String subject, String message) {
        this.title = title;
        this.message = message;
        this.subject = subject;
    }

    public Mail(String title, String message, String subject, List<String> senders, Color color) {
        this.title = title;
        this.message = message;
        this.subject = subject;
        this.senders = senders;
        this.color = color;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setSenders(List<String> senders) {
        this.senders = senders;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public String getSubject() {
        return subject;
    }

    public List<String> getSenders() {
        return senders;
    }

    public Color getColor() {
        return color;
    }

}
