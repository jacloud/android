package com.example.myapplication.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.model.Mail;

public class MailDetailViewModel extends ViewModel {

    private final MutableLiveData<Mail> selected = new MutableLiveData<Mail>();

    public void select(Mail item) {
        selected.setValue(item);
    }

    public LiveData<Mail> getSelected() {
        return selected;
    }

}
