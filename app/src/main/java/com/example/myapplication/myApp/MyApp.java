package com.example.myapplication.myApp;

import android.app.Application;
import android.graphics.Color;

import com.example.myapplication.model.Mail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyApp extends Application {

    private static List<Mail> mailList;
    private static Map<String, Integer> notificationGroup;

    // Called when the application is starting, before any other application objects have been created.
    // Overriding this method is totally optional!
    @Override
    public void onCreate() {
        super.onCreate();
        Mail m = new Mail("Welcome","Welcome to the office","Jose", new ArrayList<String>(), new Color());
        mailList = new ArrayList<>();
        mailList.add(m);
        m = new Mail("Vota por la causa que más te conmueva","Como ya sabes, destinaremos una parte de nuestros beneficios a una ONG. Debido a un fallo en el formulario que enviamos en la anterior Newsletter, tenemos que volver a repetir las votaciones.","Marc", new ArrayList<String>(), new Color());
        mailList.add(m);
        m = new Mail("¡Lunes de meditación!","Participa en las meditaciones de los lunes de 8:15h a 8:30h","Fernando", new ArrayList<String>(), new Color());
        mailList.add(m);
        m = new Mail("Evaluaciones 2019","Si tienes cualquier duda o consulta, puedes abrir una issue","Antonio", new ArrayList<String>(), new Color());
        mailList.add(m);
        m = new Mail("Escoge tu retribución flexible","Gestiona tus preferencias de retribución flexible","Eric", new ArrayList<String>(), new Color());
        mailList.add(m);
        m = new Mail("¡Ya han llegado los lotes!","Ahora ya sí que sí, ¡la navidad está a la vuelta de la esquina! ¡Y empezamos a celebrarlo con la llegada de los lotes! ","Laura", new ArrayList<String>(), new Color());
        mailList.add(m);
        m = new Mail("Tu Certificado de Cumplimiento","¡Felicidades! conseguiste tu Certificado de Cumplimiento para el curso","Udemy", new ArrayList<String>(), new Color());
        mailList.add(m);

        notificationGroup = new HashMap<>();
    }

    public static List<Mail> getMailList() {
        return mailList;
    }

    public static Map<String, Integer> getNotificationGroup() {
        return notificationGroup;
    }

}
