package com.example.myapplication.myApp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.myapplication.R;

public class NotificationHandler  extends ContextWrapper {

    private NotificationManager manager;

    private static final String CHANNEL_HIGH_NAME = "HIGH CHANNEL";
    private static final String CHANNEL_LOW_NAME = "LOW CHANNEL";

    public enum CHANNEL_ID {
        HIGH,
        LOW
    }

    public NotificationHandler(Context base) {
        super(base);
        createChannels();
    }

    public NotificationManager getManger() {
        if(manager == null) manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        return manager;
    }

    private void createChannels() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel highChannel = new NotificationChannel(CHANNEL_ID.HIGH.toString(), CHANNEL_HIGH_NAME, NotificationManager.IMPORTANCE_HIGH);
            NotificationChannel lowChannel = new NotificationChannel(CHANNEL_ID.LOW.toString(), CHANNEL_LOW_NAME, NotificationManager.IMPORTANCE_LOW);

            highChannel.enableLights(true);
            highChannel.setLightColor(Color.BLUE);

            highChannel.setShowBadge(true);
            lowChannel.setShowBadge(true);

            getManger().createNotificationChannel(highChannel);
            getManger().createNotificationChannel(lowChannel);
        }
    }

    public Notification.Builder createNotification(String title, String message, CHANNEL_ID priority) {
        return createNotification(title, message, priority.toString());
    }

    public Notification.Builder createNotificationWithIntent(String title, String message, CHANNEL_ID priority, PendingIntent pendingIntent) {
        return createNotification(title, message, priority.toString()).setContentIntent(pendingIntent).setSmallIcon(R.mipmap.ic_launcher_round).setAutoCancel(true);
    }

    private Notification.Builder createNotification(String title, String message, String channelId) {
        return create(channelId).setContentTitle(title).setContentText(message).setSmallIcon(R.mipmap.ic_launcher_round).setAutoCancel(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT_WATCH)
    public Notification.Builder createNotificationGroup(String subject, String channelId) {
        return create(channelId).setGroup(subject).setGroupSummary(true).setSmallIcon(R.mipmap.ic_launcher_round).setAutoCancel(true);
    }

    private Notification.Builder create(String channelId) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return createNotificationWithChannel(channelId);
        } else {
            return createNotificationWithoutChannel();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private Notification.Builder createNotificationWithChannel(String channelId) {
        return new Notification.Builder(getApplicationContext(), channelId);
    }

    private Notification.Builder createNotificationWithoutChannel() {
        return new Notification.Builder(getApplicationContext());
    }
}
